#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
from datetime import timedelta
from typing import Any, Union

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends
from core import security

from api.common import deps
from api.utils import response_code
from api.common.logger import logger
# from ..schemas import role_schema

# from api.models import auth
from core.config import settings

from .schemas import user_schema, token_schema
from .crud.role import curd_role

# from .crud import curd_user, curd_role

router = APIRouter()


@router.post("/login/access-token", summary="用户登录认证", response_model=token_schema.RespToken)
async def login_access_token(
) -> Any:
    """
    用户登录
    :param db:
    :param user_info:
    :return:
    """

    # 验证用户
    user = curd_user.authenticate(db, email=user_info.username, password=user_info.password)
    if not user:
        logger.info(f"用户邮箱认证错误: email{user_info.username} password:{user_info.password}")
        return response_code.resp_500(message="用户名或者密码错误")
    elif not curd_user.is_active(user):
        return response_code.resp_500(message="用户邮箱未激活")

    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)

    # 登录token 只存放了user.id
    return response_code.resp_200(data={
        "token": security.create_access_token(user.id, expires_delta=access_token_expires),
    })


@router.get("/user/info", summary="获取用户信息", response_model=user_schema.RespUserInfo)
async def get_user_info(
) -> Any:
    """
    获取用户信息
    :param db:
    :param current_user:
    :return:
    """
    role_info = curd_role.query_role(db, role_id=current_user.role_id)

    return response_code.resp_200(data={
        "role_id": current_user.role_id,
        "role": role_info.role_name,
        "nickname": current_user.nickname,
        "avatar": current_user.avatar
    })


@router.post("/user/logout", summary="用户退出", response_model=user_schema.RespBase)
async def user_logout():
    """
    用户退出
    :param token_data:
    :return:
    """
    logger.info(f"用户退出->用户id:{token_data.sub}")
    return response_code.resp_200(message="logout success")

# @router.post("/create/permission", summary="添加权限")
# async def create_permission(
#         per_info:role_schema.PermissionCreate,
#         db:Session = Depends(deps.get_db)
# ):
#     """
#     用户退出
#     :param token_data:
#     :return:
#     """
#     res = curd_role.create_permission(db=db,obj_in=per_info)
#     logger.info(f"添加权限:{res}")
#     return response_code.resp_200(message="权限添加成功")


# http://127.0.0.1:8010/api/mall/v1/admin/auth/test

@router.get("/test", summary="测试")
async def test():
    print('================')
    data = 'ヾ(๑╹◡╹)ﾉ"'
    return data
