from flask import Blueprint,request,jsonify
import json
from db import db
index_bp = Blueprint('index_bp',__name__)

@index_bp.route('/user',methods=['POST'])
def login():
    if request.method=='POST':
        data = json.loads(request.data)
        phone = data['phone']
        password = data['password']
        # 查找数据库
        sql = "select * from users where phone='%s'"%phone
        res = db.find(sql)
        if not res:
            sql = "insert into users(phone,password,type) values('%s','%s',%d)" % (phone, password, 1)
            db.update(sql)
            db.commit()
        sql = "select * from users where phone='%s'" % phone
        res = db.find(sql)
        if res:
            if password == res['password']:
                return jsonify({
                    "code": 201,
                    "msg": '登录成功',
                    'id': res['id'],
                    'phone': res['phone']
                })
        return jsonify({
            "code":400,
            "msg":'登录失败'
        })

@index_bp.route('/fabu',methods=['POST'])
def fa():
    if request.method=='POST':
        data = json.loads(request.data)
        fname = data['fname']
        context = data['context']
        uid = data['uid']

        sql = "insert into fabu(context,uid,fname) values('%s',%d,'%s')"%(context,int(uid),fname)
        db.update(sql)
        db.commit()
        return jsonify({
            "code":200,
            "msg":'发布成功'
        })

@index_bp.route('/shen',methods=['POST'])
def shen():
    if request.method == 'POST':
        data = json.loads(request.data)
        context = data['context']
        uid = data['uid']
        sid = data['sid']
        sql = "select id from fabu where context='%s'" % context
        res = db.find(sql)
        if len(context) < 10:
            text = '用户发布文章未通过'
            sql1 = "insert into shen(cid,text,uid,sid) values(%d,'%s',%d,%d)"%(int(res['id']),text,int(uid),int(sid))
            db.update(sql1)
            db.commit()
            return jsonify({
                "code":201,
                "msg":'审核未通过'
            })
        else:
            text = '发布文章通过'
            sql1 = "insert into shen(cid,text,uid,sid) values(%d,'%s',%d,%d)" % (int(res['id']), text, int(uid), int(sid))
            db.update(sql1)
            db.commit()
            score = len(context)-10
            sql2 = "update users set score=score+%d where id=%d"%(score,int(uid))
            db.update(sql2)
            db.commit()
            # 送积分
            text = 'score+'+str(score)
            sql3 = "insert into ji(cid,text,score,uid) values(%d,'%s',%d,%d)"%(int(res['id']),text,score,int(uid))
            db.update(sql3)
            db.commit()
            return jsonify({
                "code":200,
                "msg":'审核通过'
            })


@index_bp.route('/shop',methods=['POST','GET'])
def shop():
    if request.method=='POST':
        data = json.loads(request.data)
        name = data['name']
        score = int(data['score'])
        sid = int(data['sid'])
        sql = "insert into shops(name,score,sid) values('%s',%d,%d)"%(name,score,sid)
        db.update(sql)
        db.commit()
        return jsonify({
            "code":200,
            "msg":'发布商品成功'
        })
    else:
        uid = request.args.get('uid')
        sql = "select score from users where id=%d"%int(uid)
        res = db.find(sql)
        score = int(res['score'])

        sql2 = "select id,name,score from shops where score<=%d"%score
        rest = db.find_all(sql2)

        list=[]
        for i in rest:
            list.append(i)
        return jsonify({
            "code":200,
            "msg":'获取商品成功',
            'list':list
        })

@index_bp.route('/dui',methods=['POST'])
def dui():
    if request.method=='POST':
        data = json.loads(request.data)
        sid = int(data['sid'])
        uid = int(data['uid'])

        sql = "select score from users where id=%d"%uid
        res = db.find(sql)
        score = int(res['score'])
        text = '兑换成功'
        sql2 = "insert into dui(sid,text,uid) values (%d,'%s',%d)"%(sid,text,uid)
        db.update(sql2)

        sql4 = "select score from shops where id=%d"%sid
        rest = db.find(sql4)
        score1 = int(rest['score'])
        sql3 = "update users set score=score-%d where id=%d"%(score1,int(uid))
        db.update(sql3)
        db.commit()
        return jsonify({
            "code":200,
            "msg":'兑换成功'
        })



@index_bp.route('/text')
def text():
    list = [
        {
            'id':1,
            'name':'电器',
            'pid':0
        },
        {
            'id':2,
            'name':'空调',
            'pid':1
        },
        {
            'id':3,
            'name':'手机',
            'pid':0,
        },
        {
            'id':4,
            'name':'vivo手机',
            'pid':3
        }
    ]

    dict = {}
    list1 = []
    for i in list:
        dict[i['id']] = i
    for j in list:
        pid = j['pid']
        if int(pid)==0:
            list1.append(j)
        else:
            if 'son' not in dict[pid]:
                dict[pid]['son'] = []

            dict[pid]['son'].append(j)

    return jsonify({
         "code":200,
         "list":list1
     })