#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
from datetime import timedelta
from typing import Any, Union

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends
from core import security

from api.common import deps
from api.utils import response_code
from api.common.logger import logger

# from api.models import auth
from core.config import settings
import time


from .schemas import user_schema, token_schema
from .crud.user import curd_user

from api.utils.captcha.captcha.captcha import captcha
from flask import make_response
from api.utils.my_redis import r
from api.utils.my_rong import sms
import random
from api.utils.my_jwt import jwt_pay


# from .crud import curd_user, curd_role

router = APIRouter()



@router.get("/user/image", summary="获取图片验证码")
async def get_user_image(
        db:Session = Depends(deps.get_db),
        uuid:str='',


) -> Any:
    """
    获取用户信息
    :param db:
    :param current_user:
    :return:
    """
    image = curd_user.imageurl(uuid=uuid)
    return image



# 发送短信
@router.post("/send/messgae", summary="发送短信")
async def login_access_token(
        user_info:user_schema.Message,
        db:Session = Depends(deps.get_db)
) -> Any:
    send_message = curd_user.send_message(mobile=user_info.username,code=user_info.code,uuid=user_info.uuid)
    if send_message == '1001':
        return response_code.resp_500(message='手机号错误')
    elif send_message == '1003':
        return response_code.resp_500(message='1分钟之内不能重复发')
    elif send_message == '1002':
        return response_code.resp_500(message='图形验证码错误')
    return response_code.resp_200(message='发送成功，注意查收')

# 用户登录
@router.post("/login/access-token", summary="用户登录")
async def login_access_token(
        user_info:user_schema.UserPhoneAuth,
        db:Session = Depends(deps.get_db)
) -> Any:
    """
    用户登录
    :param db:
    :param user_info:用户账号信息
    :return:
    """
    # 验证输入信息
    recode = curd_user.username_verify(account=user_info.username,password=user_info.password,code=user_info.code)
    if recode=='1001':
        return response_code.resp_500(message='用户名或者密码为空')
    elif recode=='1002':
        return response_code.resp_500(message='手机号错误')
    elif recode=='1003':
        return response_code.resp_500(message='验证码错误')
    print('111')
    # 验证用户
    user = curd_user.authenticate(db,account=user_info.username, password=user_info.password)
    print('222')
    # 没有用户
    if not user:
        # logger.info(f"用户邮箱认证错误: email{username} password:{password}")
        # 创建用户
        user_create = curd_user.create(db,obj_in=user_info)
        logger.info(f"创建用户: account{user_info.username} password:{user_info.username}")
        return response_code.resp_500(messgae='请注册用户')

    elif user == '1002':
        logger.info(f"密码错误: email{user_info.username} password:{user_info.password}")
        return response_code.resp_500(message="密码错误")
    # 生成jwt
    data = {
        'id':user.id,
        'role':user.role_id,
        'exp':int(time.time())+5400
    }
    token = jwt_pay.jwt_encode(data)
    # 登录token 只存放了user.id
    return response_code.resp_200(data={
        "userid":user.id,
        "token": token
    })







# @router.get("/user/info", summary="获取用户信息", response_model=user_schema.RespUserInfo)
# async def get_user_info(
# ) -> Any:
#     """
#     获取用户信息
#     :param db:
#     :param current_user:
#     :return:
#     """
#     role_info = curd_role.query_role(db, role_id=current_user.role_id)
#
#     return response_code.resp_200(data={
#         "role_id": current_user.role_id,
#         "role": role_info.role_name,
#         "nickname": current_user.nickname,
#         "avatar": current_user.avatar
#     })


@router.post("/user/logout", summary="用户退出", response_model=user_schema.RespBase)
async def user_logout():
    """
    用户退出
    :param token_data:
    :return:
    """
    logger.info(f"用户退出->用户id:{token_data.sub}")
    return response_code.resp_200(message="logout success")


# http://127.0.0.1:8010/api/mall/v1/admin/auth/test

@router.get("/test", summary="测试")
async def test():
    print('================')
    data = 'ヾ(๑╹◡╹)ﾉ"'
    return data
