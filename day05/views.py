#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
from datetime import timedelta
from typing import Any, Union

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends,Query
from core import security

from api.common import deps
from api.utils import response_code
from api.common.logger import logger

from api.models import auth
from core.config import settings

from .schemas import user_schema, token_schema,role_schema

from .crud import curd_user, curd_role

router = APIRouter()

#添加角色
@router.post("/add/addrole", summary="添加角色")
async def add_category(
        role_info: role_schema.RoleCreate,
        db: Session = Depends(deps.get_db),# 指定数据库
        # token_data: Union[str, Any] = Depends(deps.check_jwt_token),
):
    logger.info(f"添加角色名:{role_info.name}")
    curd_role.create(db=db, obj_in=role_info)
    return response_code.resp_200(message="角色添加成功")

# 获取基础角色
@router.get("/get/jichu", summary="获取基础角色")
async def add_category(
        db: Session = Depends(deps.get_db),# 指定数据库
):
    respq = curd_role.get_jichu(db=db)
    logger.info(f"获取基础角色:{respq}")
    return response_code.resp_200(data=respq)

#获取角色列表
@router.get("/getrole", summary="查询分类")
async def getrole(
        db: Session = Depends(deps.get_db),
        page:int=Query(1,ge=1,title='当前页'),
        page_size:int=Query(10,le=50,title='页码长度')
):

    response_result = curd_role.queryall_role(db,page=page,page_size=page_size)['items']
    total = curd_role.queryall_role(db,page=page,page_size=page_size)['total']
    logger.info(f"获取角色{response_result}")
    return response_code.resp_200(data={'resp':response_result,'total':total})
#添加资源
@router.post("/add/resource", summary="添加资源")
async def add_category(
        resour_info: role_schema.ResourceCreate,
        db: Session = Depends(deps.get_db),# 指定数据库
):
    logger.info(f"添加资源名:{resour_info.name}")
    curd_role.create_resource(db=db, obj_in=resour_info)
    return response_code.resp_200(message="资源添加成功")

# 获取资源
@router.get("/rescours/pid", summary="资源分类")
async def getrole(
        db: Session = Depends(deps.get_db),
        pid:int= Query(...,title='查看级别')
):
    logger.info(f'查看级别:{pid}')
    rescours_pid=curd_role.getpidadd_rescource(db=db,pid=pid)['items']
    return response_code.resp_200(data=rescours_pid)
#获取资源列表
@router.get("/getrescours", summary="获取资源页面")
async def getrole(
        db: Session = Depends(deps.get_db),
        page:int=Query(1,ge=1,title='当前页'),
        page_size:int=Query(10,le=50,title='页码长度')
):

    response_result = curd_role.queryall_resource(db,page=page,page_size=page_size)['items']
    total = curd_role.queryall_resource(db,page=page,page_size=page_size)['total']
    logger.info(f"获取资源{response_result}")
    return response_code.resp_200(data={'resp':response_result,'total':total})

# 查找角色
@router.get("/getonerole", summary="查询分类")
async def getonerole(
        db: Session = Depends(deps.get_db),
        name:str=Query(...,title='角色名称')
):

    response_result = curd_role.query_getrole(db,name=name)['items']
    logger.info(f"查找角色{response_result}")
    return response_code.resp_200(data=response_result)

# 获取二级资源
@router.get("/gettworescours", summary="获取资源页面")
async def gettworescours(
        db: Session = Depends(deps.get_db)
):

    response_result = curd_role.querytwo_Resource(db)['items']
    logger.info(f"获取二级资源{response_result}")
    return response_code.resp_200(data=response_result)

# 获取角色资源
@router.get("/getrolerescours", summary="获取角色资源")
async def gettworescours(
        db: Session = Depends(deps.get_db),
        roleid:int=0
):

    response_result = curd_role.query_StudentTeacher(db,roleid=roleid)['items']
    logger.info(f"获取角色资源{response_result}")
    return response_code.resp_200(data=response_result)

#角色配置资源
@router.get("/setResource", summary="添加角色资源")
async def setrolerescours(
        # resource_info:role_schema.StudentCreate,
        db: Session = Depends(deps.get_db),
        roleid:int=0,
        resourceid:str=''
):
    response_result = curd_role.queryset_StudentTeacher(db,roleid=roleid,resourceid=resourceid)
    logger.info(f"添加资源{response_result}")
    return response_code.resp_200(message='11')

