#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
from datetime import timedelta
from typing import Any, Union

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends,Query
from core import security

from api.common import deps
from api.utils import response_code
from api.common.logger import logger

from api.models import auth
from core.config import settings

from .schemas import user_schema, token_schema,role_schema

from .crud import curd_user, curd_role

router = APIRouter()

#添加角色
@router.post("/add/addrole", summary="添加角色")
async def add_category(
        role_info: role_schema.RoleCreate,
        db: Session = Depends(deps.get_db),# 指定数据库
):
    logger.info(f"添加角色名:{role_info.name}")
    curd_role.create(db=db, obj_in=role_info)
    return response_code.resp_200(message="角色添加成功")

# 获取基础角色
@router.get("/get/jichu", summary="获取基础角色")
async def add_category(
        db: Session = Depends(deps.get_db),# 指定数据库
):
    respq = curd_role.get_jichu(db=db)
    logger.info(f"获取基础角色:{respq}")
    return response_code.resp_200(data=respq)

#获取角色列表
@router.get("/getrole", summary="查询分类")
async def getrole(
        db: Session = Depends(deps.get_db),
        page:int=Query(1,ge=1,title='当前页'),
        page_size:int=Query(10,le=50,title='页码长度')
):

    response_result = curd_role.queryall_role(db,page=page,page_size=page_size)['items']
    total = curd_role.queryall_role(db,page=page,page_size=page_size)['total']
    logger.info(f"获取角色{response_result}")
    return response_code.resp_200(data={'resp':response_result,'total':total})

#添加资源
@router.post("/add/resource", summary="添加资源")
async def add_category(
        resour_info: role_schema.ResourceCreate,
        db: Session = Depends(deps.get_db),# 指定数据库
):
    logger.info(f"添加资源名:{resour_info.name}")
    curd_role.create_tworesource(db=db, obj_in=resour_info)
    return response_code.resp_200(message="资源添加成功")

def regroup(data):
    # 进行数据重组
    idlist = []
    reslist = []
    for its in data:
        if its['id'] not in idlist:
            reslist.append({'id': its['id'], 'name': its['pname'],
                            'son': [{'id': its['rid'], 'name': its['rname'], 'url': its['url']}]})
            idlist.append(its['id'])
        else:
            index = idlist.index(its['id'])
            reslist[index]['son'].append({'id': its['rid'], 'name': its['rname'], 'url': its['url']})
    return reslist
# 获取侧边栏
@router.get("/get/left", summary="用户资源")
async def get_left(
        db: Session = Depends(deps.get_db),# 指定数据库
        roleid:int=0, # 角色id
):
    logger.info(f"获取角色资源:{roleid}")
    resp=curd_role.gettwo_rolerescource(db=db, roleid=roleid)
    # 数据重组
    data = regroup(resp)
    print('333', data)
    return response_code.resp_200(data=data)

# 获取资源
@router.get("/rescours/pid", summary="资源分类")
async def getrole(
        db: Session = Depends(deps.get_db),
        pid:int= Query(...,title='查看级别')
):
    logger.info(f'查看级别:{pid}')
    rescours_pid=curd_role.getpidadd_rescource(db=db,pid=pid)['items']
    return response_code.resp_200(data=rescours_pid)


#获取资源列表
@router.get("/getrescours", summary="获取资源页面")
async def getrole(
        db: Session = Depends(deps.get_db),
        page:int=Query(1,ge=1,title='当前页'),
        page_size:int=Query(10,le=50,title='页码长度')
):

    response_result = curd_role.queryall_resource(db,page=page,page_size=page_size)['items']
    total = curd_role.queryall_resource(db,page=page,page_size=page_size)['total']
    logger.info(f"获取资源{response_result}")
    return response_code.resp_200(data={'resp':response_result,'total':total})

# 查找角色
@router.get("/getonerole", summary="查询分类")
async def getonerole(
        db: Session = Depends(deps.get_db),
        name:str=Query(...,title='角色名称')
):

    response_result = curd_role.query_getrole(db,name=name)['items']
    logger.info(f"查找角色{response_result}")
    return response_code.resp_200(data=response_result)

# 获取二级资源
@router.get("/gettworescours", summary="获取资源页面")
async def gettworescours(
        db: Session = Depends(deps.get_db)
):

    response_result = curd_role.querytwo_Resource(db)['items']
    logger.info(f"获取二级资源{response_result}")
    return response_code.resp_200(data=response_result)

# 获取角色资源
@router.get("/getrolerescours", summary="获取角色资源")
async def gettworescours(
        db: Session = Depends(deps.get_db),
        roleid:int=0
):

    response_result = curd_role.querytwo_StudentTeacher(db,roleid=roleid)
    logger.info(f"获取角色资源{response_result}")
    return response_code.resp_200(data=response_result)

#角色配置资源
@router.get("/setResource", summary="添加角色资源")
async def setrolerescours(
        # resource_info:role_schema.StudentCreate,
        db: Session = Depends(deps.get_db),
        roleid:int=0,
        resourceid:str=''
):
    response_result = curd_role.addrole_rescourse(db,roleid=roleid,resourceid=resourceid)
    logger.info(f"添加资源{response_result}")
    if response_result:
        return response_code.resp_200(message='资源添加成功')
    else:
        return response_code.resp_400(message='资源互斥不能添加')


# 工作流创建
@router.post("/add/work", summary="添加工作流")
async def setrolerescours(
        work_info:role_schema.WorkCreate,
        db: Session = Depends(deps.get_db),
):
    response_result = curd_role.create_work(db,obj_in=work_info)
    logger.info(f"添加工作流{response_result}")

    return response_code.resp_200(data=response_result)

# 查看工作流
@router.get("/get/work", summary="查看工作流")
async def setrolerescours(
        # work_info:role_schema.WorkCreate,
        db: Session = Depends(deps.get_db),
):
    response_result = curd_role.get_work(db)
    logger.info(f"查看工作流{response_result}")

    return response_code.resp_200(data=response_result)

# 用户登录
@router.post("/login/access-token", summary="用户登录认证", response_model=token_schema.RespToken)
async def login_access_token(
        *,
        db: Session = Depends(deps.get_db),
        user_info: user_schema.UserEmailAuth,
) -> Any:
    """
    用户登录
    :param db:
    :param user_info:
    :return:
    """

    # 验证用户
    user = curd_user.authenticate(db, username=user_info.username, password=user_info.password)
    print('333',user)
    if not user:
        logger.info(f"用户邮箱认证错误: email{user_info.username} password:{user_info.password}")
        return response_code.resp_500(message="用户名或者密码错误")
    # elif not curd_user.is_active(user):
    #     return response_code.resp_500(message="用户未创建")

    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)

    # 登录token 只存放了user.id
    return response_code.resp_200(data={
        "token": security.create_access_token(user.id, expires_delta=access_token_expires),
        "username":user_info.username,
        "roleid":user.rid
    })
